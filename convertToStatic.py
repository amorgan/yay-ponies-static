#!/bin/env python3
# Let the sun rise!
import glob,os,re
from shutil import copyfile

# Make sure this is set correctly!
site_name = "153Xfb5Cddgb9Jr7zYFypJRDL5BxmCoQRV"

def main():
    # Bail out if script has already been ran or site
    # has not been downloaded properly
    if not os.path.isfile('index.php'):
        print ('Error: You are not running from the site\'s root dir, haven\'t cloned YP\'s repo, or have already run this script!')
        return 1

    # Remove ZN generated index.html
    if os.path.isfile('index.html'):
        os.remove('index.html')

    # Remove permalink file, it's not necessary and breaks script
    if os.path.isfile('permalink.php'):
        os.remove('permalink.php')

    # Change all *.php to *.html cuz ZeroNet
    directory = os.getcwd()
    for subdir, dirs, files in os.walk(directory):
        for filename in files:
            if filename.find('.php') > 0:
                subdirectoryPath = os.path.relpath(subdir, directory) #get the path to your subdirectory
                filePath = os.path.join(subdirectoryPath, filename) #get the path to your file
                newFilePath = filePath.replace(".php",".html") #create the new name
                os.rename(filePath, newFilePath) #rename your file

    # Process SSI and preceed all local instances with site name
    replacements = {'href="/':'href="/' + site_name + '/','src="/':'src="/' + site_name + '/'}

    print('This should be site root (e.g. /var/lib/zeronet/153Xfb5Cddgb9Jr7zYFypJRDL5BxmCoQRV): ' + os.getcwd())

    # First fix all ZN-specific stuff in files
    for filename in glob.iglob('**/*.html', recursive=True):
        lines = []
        print(filename)
        with open(filename) as the_file:
            for line in the_file:
                #Preceed local paths
                for src, target in replacements.items():
                    line = line.replace(src, target)

                # Remove permalink as a middle man
                if 'permalink.php?link=' in line:
                    line = line.replace('permalink.php?link=','')
                    line = line.replace('">','.html">')

                # TODO: Get working
                # Remove ponygetters
                if ('"ponygetter"' in line) or ('"bookgetter"' in line):
                    line = '<!--' + line

                if '</form>' in line:
                    line = line + '-->'

                # Change all mentions of php to html
                if 'php' in line:
                    line = line.replace('php', 'html')

                lines.append(line)
            with open(filename, 'w') as outfile:
                for line in lines:
                    outfile.write(line)

    # Now actually carry out the SSI pre-processing
    for filename in glob.iglob('**/*.html', recursive=True):
        lines = []
        with open(filename) as the_file:
            for line in the_file:
                if '<!--#include file=' in line:
                    match = re.search('#include file="(.+?)" -->', line)
                    if match:
                        match_filename = match.groups()[0]
                        # Remove preceeding "/"
                        if match_filename.startswith("/"):
                            match_filename = match_filename[1:]
                        else:
                            file_path = os.path.dirname(os.path.realpath(filename))
                            match_filename = file_path + '/' + match_filename
                        line = open(match_filename).read()

                lines.append(line)
            with open(filename, 'w') as outfile:
                for line in lines:
                    outfile.write(line)

    print ('Done!')

if __name__ == "__main__":
    main()
